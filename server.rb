# frozen_string_literal: true

require 'sinatra'

get '/' do
  headers \
    'Content-Security-Policy' => "style-src 'nonce-#{params[:headernonce]}'; report-uri /csp-report",
    'Content-Type' => 'text/html'

  <<~HTML
  <html>
    <head>
      <link rel="stylesheet" href="/style.css" nonce="#{params[:bodynonce]}" />
      <style nonce="#{params[:bodynonce]}">h1 { color: green }></style>
    </head>
    <body> 
      <h1>test</h1>
    </body>
  </html>
  HTML
end

get '/style.css' do
  headers 'Content-Type' => 'text/css'

  'body { background-color: red  }'
end

post '/csp-report' do
  puts request.path_info
  puts request.body.read
end

get '/form-action' do
  headers \
    'Content-Security-Policy' => "form-action 'self'; report-uri /csp-report",
    'Content-Type' => 'text/html'

  <<~HTML
  <html>
    <head></head>
    <body>
      <form action="/redirect">
        <input type="submit">
      </form>
    </body>
  </html>
  HTML
end

get '/redirect' do
  redirect 'customscheme://abc?adsfsd=3'
end
